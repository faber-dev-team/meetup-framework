# Faber Dev Meetup Framework

Use the following ***Code of Conduct*** to guide your next meetup/event.

## Code of Conduct

### Sponsors / Partnerships
* Clearly communicate to everyone who the sponsors/partners for the event are.
* Make sure there are no conflicts of interest.

### Community
*  ***Focus:*** Focus on the community and the rest shall come. 
*  ***Priorities:*** Promoting communication, knowledge and networking are the very top priorities for our events. Here, people have the stand and companies listen.
*  ***Schedule:*** All Faber events are independent and organized by different people or teams internally, hence, events may overlap as long as quality is assured.
*  ***Hiring:*** We understand that most companies need to hire at some point or another, however, it's better if you step up and give a talk about your company and the cool things you do than approaching people with the sole purpose of hiring.
*  ***Visibility:*** Sponsors, Partners and Organizers should only act as facilitators, the focus is on the people, the topics and the speakers. 

### Contacts
Each event should have at least one designated organizer, responsible for answering all questions. Make sure their contacts are publicly available.  


---

## Checklist: 

Make sure the following checklist is followed and shared among speakers and organizers.

### Meetup
- [ ] Share the meetup name, description and link to online page

### 1. Venue
#### a) Location
- [ ] Address, and how to get there

#### b) Disposition
- [ ] Describe how's the room setup

#### c) Attendance
- [ ] Describe how many people you expect to show up  
- [ ] Describe the type of audience

### 2. Format
- [ ] Describe how this meetup will organized, pannel, talks, workshop, etc.

### 3. Image, Sound, etc
- [ ] Ask about if any of the speakers needs a digital projector, a tv, sound system, etc.

### 4. Schedule
- [ ] Define starting and ending times  
- [ ] Make sure you speakers can come in a bit earlier if they want

### 5. Social Media Channels
- [ ] Twitter and Linkedin  
- [ ] Internal Portfolio Slack